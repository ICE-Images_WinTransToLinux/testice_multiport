#include<Ice/Ice.h>
#include<string>
#include<stdio.h>
#include<iostream>
#include"Printer.h"

using namespace std;
using namespace Demo;
int main(int argc, char *argv[])
{
	string paraIP, msg;
	msg = string("Listen: ");
	//if (argc == 3) //argc=4 with msg
	//{
	//	if (*argv[1] = '-' && *(argv[1] + 1) == 'h')
	//		paraIP = string("-h ") + argv[2];
	//}
	if (argc > 1) //get msg
	{
		char buff[500];
		sprintf(buff, "%s", argv[1]);
		msg += buff;
	}
	else
	{
		cout << "Usage: cmd msg" << endl;
		return 0;
	}
	int status = 0;
	Ice::CommunicatorPtr ic;
	try {

		Ice::InitializationData initialData;
		initialData.properties = Ice::createProperties();
		initialData.properties->load("init.config");
		ic = Ice::initialize(initialData);
		paraIP += "-h localhost";
		string Pro_para("SimplePrinter:default -p 10000 ");
		if (!paraIP.empty())
			Pro_para += paraIP;
		Ice::ObjectPrx base = ic->stringToProxy(Pro_para.c_str()); //-h 172.16.37.27 as argv
		PrinterPrx printer = PrinterPrx::checkedCast(base);//�ӿڴ���
		if (!printer)
			throw "Invalid proxy";
		//printer->printString("Hello World!");
		printer->printString(msg.c_str());
		
		//ic->shutdown();
	}
	catch (const Ice::Exception& ex) {
		cerr << ex << endl;
		status = 1;
	}
	catch (const char* msg) {
		cerr << msg << endl;
		status = 1;
	}
	if (status)
		system("pause");
	if (ic)
	ic->destroy();

	return status;
}
