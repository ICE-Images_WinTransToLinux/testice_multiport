#include<iostream>
#include<stdlib.h>

//ICE test helloWorld server
#include<Ice/Ice.h>
#include "Printer.h"
#include<string>

using namespace std;
using namespace Demo;

//to get Printer.cpp -- should compile Printer.cpp in ice3.6.1/include
//slice2cpp Printer.ice -> Printer.cpp
class PrinterI : public Printer {
public:
	virtual void printString(const string& s,
		const Ice::Current&);
};

void PrinterI::printString(const string& s, const Ice::Current&)
{
	cout << s << endl;
}
//-------------------------------
int ICEserver_t()
{
	int status = 0;
	Ice::CommunicatorPtr ic;
	try{
		Ice::InitializationData initData;
		initData.properties = Ice::createProperties();
		initData.properties->load("init.config");
		ic = Ice::initialize(initData);
		Ice::ObjectAdapterPtr adapter
			= ic->createObjectAdapterWithEndpoints(
			"SimplePrinterAdapter", "default -p 10000");
		Ice::ObjectPtr object = new PrinterI; // class PrinterI
		adapter->add(object, ic->stringToIdentity("SimplePrinter"));
		adapter->activate();
		ic->waitForShutdown();

	}
	catch (const Ice::Exception& e){
		cerr << e << endl;
		status = 1;
	}
	catch (const char* msg){
		cerr << msg << endl;
		status = 1;
	}

	if (ic) {
		try {
			ic->destroy();
		}
		catch (const Ice::Exception& e) {
			cerr << e << endl;
			status = 1;
		}
	}
	return status;
}
int main()
{
	cout << "start server" << endl;
	ICEserver_t();
	return 0;
}
